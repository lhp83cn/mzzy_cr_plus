/*
 * 项目名称:mzzy-plus
 * 类名称:DataSource.java
 * 包名称:com.hackbuteer.mzzy.datasources.annotation
 *
 * 修改履历:
 *      日期                修正者      主要内容
 *      2018/11/21 16:04    铭众      初版完成
 *
 * Copyright (c) 2019-2019 铭众智远软件
 */
package com.hackbuteer.mzzy.datasources.annotation;

import java.lang.annotation.*;

/**
 * 多数据源注解
 *
 * @author 铭众
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {
    String name() default "";
}
