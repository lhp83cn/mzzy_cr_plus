/*
 * 项目名称:mzzy-plus
 * 类名称:DataSourceNames.java
 * 包名称:com.hackbuteer.mzzy.datasources
 *
 * 修改履历:
 *      日期                修正者      主要内容
 *      2018/11/21 16:04    铭众      初版完成
 *
 * Copyright (c) 2019-2019 铭众智远软件
 */
package com.hackbuteer.mzzy.datasources;

/**
 * 增加多数据源
 *
 * @author 铭众
 */
public interface DataSourceNames {
    String FIRST = "first";
    String SECOND = "second";
}
