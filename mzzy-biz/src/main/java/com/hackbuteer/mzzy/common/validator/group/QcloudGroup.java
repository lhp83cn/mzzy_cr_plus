/*
 * 项目名称:platform-plus
 * 类名称:QcloudGroup.java
 * 包名称:com.platform.common.validator.group
 *
 * 修改履历:
 *      日期                修正者      主要内容
 *      2018/11/21 16:04    铭众      初版完成
 *
 * Copyright (c) 2019-2019 铭众智远软件
 */
package com.hackbuteer.mzzy.common.validator.group;

/**
 * 腾讯云
 *
 * @author 铭众
 */
public interface QcloudGroup {
}
