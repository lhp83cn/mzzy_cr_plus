/*
 * 项目名称:mzzy-plus
 * 类名称:SysOssServiceImpl.java
 * 包名称:com.hackbuteer.mzzy.modules.oss.service.impl
 *
 * 修改履历:
 *      日期                修正者      主要内容
 *      2019/1/17 16:21    铭众      初版完成
 *
 * Copyright (c) 2019-2019 铭众智远软件
 */
package com.hackbuteer.mzzy.modules.oss.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hackbuteer.mzzy.common.utils.Query;
import com.hackbuteer.mzzy.modules.oss.dao.SysOssDao;
import com.hackbuteer.mzzy.modules.oss.entity.SysOssEntity;
import com.hackbuteer.mzzy.modules.oss.service.SysOssService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 铭众
 */
@Service("sysOssService")
public class SysOssServiceImpl extends ServiceImpl<SysOssDao, SysOssEntity> implements SysOssService {

    @Override
    public Page queryPage(Map<String, Object> params) {
        //排序
        params.put("sidx", "T.CREATE_DATE");
        params.put("asc", false);
        Page<SysOssEntity> page = new Query<SysOssEntity>(params).getPage();
        return page.setRecords(baseMapper.selectSysOssPage(page, params));
    }
}
