package com.hackbuteer.mzzy.modules.swaggerbootstrapui.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author 铭众
 */
@Configuration
@ComponentScan(
        basePackages = {"com.hackbuteer.mzzy.modules.swaggerbootstrapui.web"}
)
public class SwaggerBootstrapUiConfiguration {

}
