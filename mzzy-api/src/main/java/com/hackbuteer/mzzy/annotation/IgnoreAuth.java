/*
 * 项目名称:hackbuteer.mzzy-plus
 * 类名称:Login.java
 * 包名称:com.hackbuteer.mzzy.modules.app.annotation
 *
 * 修改履历:
 *      日期                修正者      主要内容
 *      2018/11/21 16:04    铭众      初版完成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.hackbuteer.mzzy.annotation;

import java.lang.annotation.*;

/**
 * app登录效验
 *
 * @author 铭众
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreAuth {
}
