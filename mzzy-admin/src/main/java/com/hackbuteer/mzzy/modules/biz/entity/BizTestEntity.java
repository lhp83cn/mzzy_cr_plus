/*
 * 项目名称:mzzy-plus
 * 类名称:BizTestEntity.java
 * 包名称:com.hackbuteer.mzzy.modules.biz.entity
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2021-06-05 16:00:57        高建文     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.hackbuteer.mzzy.modules.biz.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import lombok.Data;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 实体
 *
 * @author 高建文
 * @date 2021-06-05 16:00:57
 */
@Data
@TableName("BIZ_TEST")
public class BizTestEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    private Integer id;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 生日
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    /**
     * 分数
     */
    private BigDecimal score;
    /**
     * 姓名
     */
    private String name;
}
