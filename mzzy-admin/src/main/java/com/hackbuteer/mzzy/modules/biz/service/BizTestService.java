/*
 * 项目名称:mzzy-plus
 * 类名称:BizTestService.java
 * 包名称:com.hackbuteer.mzzy.modules.biz.service
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2021-06-05 16:00:57        高建文     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.hackbuteer.mzzy.modules.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hackbuteer.mzzy.modules.biz.entity.BizTestEntity;

import java.util.List;
import java.util.Map;

/**
 * Service接口
 *
 * @author 高建文
 * @date 2021-06-05 16:00:57
 */
public interface BizTestService extends IService<BizTestEntity> {

    /**
     * 查询所有列表
     *
     * @param params 查询参数
     * @return List
     */
    List<BizTestEntity> queryAll(Map<String, Object> params);

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return Page
     */
    Page queryPage(Map<String, Object> params);

    /**
     * 新增
     *
     * @param bizTest 
     * @return 新增结果
     */
    boolean add(BizTestEntity bizTest);

    /**
     * 根据主键更新
     *
     * @param bizTest 
     * @return 更新结果
     */
    boolean update(BizTestEntity bizTest);

    /**
     * 根据主键删除
     *
     * @param id id
     * @return 删除结果
     */
    boolean delete(Integer id);

    /**
     * 根据主键批量删除
     *
     * @param ids ids
     * @return 删除结果
     */
    boolean deleteBatch(Integer[] ids);
}
