/*
 * 项目名称:mzzy-plus
 * 类名称:SysUserRoleDao.java
 * 包名称:com.hackbuteer.mzzy.modules.sys.dao
 *
 * 修改履历:
 *      日期                修正者      主要内容
 *      2018/11/21 16:04    铭众      初版完成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.hackbuteer.mzzy.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hackbuteer.mzzy.modules.sys.entity.SysUserRoleEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 * @author 铭众
 */
@Mapper
public interface SysUserRoleDao extends BaseMapper<SysUserRoleEntity> {

    /**
     * 根据用户ID，获取角色ID列表
     *
     * @param userId 用户ID
     * @return List
     */
    List<String> queryRoleIdList(String userId);


    /**
     * 根据角色ID数组，批量删除
     *
     * @param roleIds 角色ids
     * @return int
     */
    int deleteBatch(String[] roleIds);
}
