/*
 * 项目名称:mzzy-plus
 * 类名称:BizTestServiceImpl.java
 * 包名称:com.hackbuteer.mzzy.modules.biz.service.impl
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2021-06-05 16:00:57        高建文     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.hackbuteer.mzzy.modules.biz.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hackbuteer.mzzy.common.utils.Query;
import com.hackbuteer.mzzy.modules.biz.dao.BizTestDao;
import com.hackbuteer.mzzy.modules.biz.entity.BizTestEntity;
import com.hackbuteer.mzzy.modules.biz.service.BizTestService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Service实现类
 *
 * @author 高建文
 * @date 2021-06-05 16:00:57
 */
@Service("bizTestService")
public class BizTestServiceImpl extends ServiceImpl<BizTestDao, BizTestEntity> implements BizTestService {

    @Override
    public List<BizTestEntity> queryAll(Map<String, Object> params) {
        return baseMapper.queryAll(params);
    }

    @Override
    public Page queryPage(Map<String, Object> params) {
        //排序
        params.put("sidx", "T.id");
        params.put("asc", false);
        Page<BizTestEntity> page = new Query<BizTestEntity>(params).getPage();
        return page.setRecords(baseMapper.selectBizTestPage(page, params));
    }

    @Override
    public boolean add(BizTestEntity bizTest) {
        return this.save(bizTest);
    }

    @Override
    public boolean update(BizTestEntity bizTest) {
        return this.updateById(bizTest);
    }

    @Override
    public boolean delete(Integer id) {
        return this.removeById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(Integer[] ids) {
        return this.removeByIds(Arrays.asList(ids));
    }
}
