/*
 * 项目名称:mzzy-plus
 * 类名称:SysLogDao.java
 * 包名称:com.hackbuteer.mzzy.modules.sys.dao
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-01-22 10:36:49        铭众     初版做成
 *
 * Copyright (c) 2018-2019 微同软件
 */
package com.hackbuteer.mzzy.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hackbuteer.mzzy.modules.sys.entity.SysLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志Dao
 *
 * @author 铭众
 * @date 2019-01-22 10:36:49
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {

}
