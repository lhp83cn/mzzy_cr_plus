/*
 * 项目名称:mzzy-plus
 * 类名称:ScheduleRunnable.java
 * 包名称:com.hackbuteer.mzzy.modules.job.utils
 *
 * 修改履历:
 *      日期                修正者      主要内容
 *      2018/11/21 16:04    铭众      初版完成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.hackbuteer.mzzy.modules.job.utils;

import com.hackbuteer.mzzy.common.exception.BusinessException;
import com.hackbuteer.mzzy.common.utils.SpringContextUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;

/**
 * 执行定时任务
 *
 * @author 铭众
 */
public class ScheduleRunnable implements Runnable {
    private Object target;
    private Method method;
    private String params;

    public ScheduleRunnable(String beanName, String methodName, String params) throws NoSuchMethodException, SecurityException {
        this.target = SpringContextUtils.getBean(beanName);
        this.params = params;

        if (StringUtils.isNotBlank(params)) {
            this.method = target.getClass().getDeclaredMethod(methodName, String.class);
        } else {
            this.method = target.getClass().getDeclaredMethod(methodName);
        }
    }

    @Override
    public void run() {
        try {
            ReflectionUtils.makeAccessible(method);
            if (StringUtils.isNotBlank(params)) {
                method.invoke(target, params);
            } else {
                method.invoke(target);
            }
        } catch (Exception e) {
            throw new BusinessException("执行定时任务失败", e);
        }
    }

}
