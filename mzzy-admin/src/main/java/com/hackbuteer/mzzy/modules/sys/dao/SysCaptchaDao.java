/*
 * 项目名称:mzzy-plus
 * 类名称:SysCaptchaDao.java
 * 包名称:com.hackbuteer.mzzy.modules.sys.dao
 *
 * 修改履历:
 *      日期                修正者      主要内容
 *      2018/11/21 16:04    铭众      初版完成
 *
 * Copyright (c) 2019-2019 铭众智远软件
 */
package com.hackbuteer.mzzy.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hackbuteer.mzzy.modules.sys.entity.SysCaptchaEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 验证码
 *
 * @author 铭众
 */
@Mapper
public interface SysCaptchaDao extends BaseMapper<SysCaptchaEntity> {

}
