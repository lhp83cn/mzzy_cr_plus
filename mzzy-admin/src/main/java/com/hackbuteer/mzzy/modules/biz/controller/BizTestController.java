/*
 * 项目名称:mzzy-plus
 * 类名称:BizTestController.java
 * 包名称:com.hackbuteer.mzzy.modules.biz.controller
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2021-06-05 16:00:57        高建文     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.hackbuteer.mzzy.modules.biz.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hackbuteer.mzzy.common.annotation.SysLog;
import com.hackbuteer.mzzy.common.utils.RestResponse;
import com.hackbuteer.mzzy.modules.sys.controller.AbstractController;
import com.hackbuteer.mzzy.modules.biz.entity.BizTestEntity;
import com.hackbuteer.mzzy.modules.biz.service.BizTestService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Controller
 *
 * @author 高建文
 * @date 2021-06-05 16:00:57
 */
@RestController
@RequestMapping("/biz/test")
public class BizTestController extends AbstractController {
    @Autowired
    private BizTestService bizTestService;

    /**
     * 查看所有列表
     *
     * @param params 查询参数
     * @return RestResponse
     */
    @RequestMapping("/queryAll")
    @RequiresPermissions("biz:test:list")
    public RestResponse queryAll(@RequestParam Map<String, Object> params) {
        List<BizTestEntity> list = bizTestService.queryAll(params);

        return RestResponse.success().put("list", list);
    }

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return RestResponse
     */
    @GetMapping("/list")
    @RequiresPermissions("biz:test:list")
    public RestResponse list(@RequestParam Map<String, Object> params) {
        Page page = bizTestService.queryPage(params);

        return RestResponse.success().put("page", page);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return RestResponse
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("biz:test:info")
    public RestResponse info(@PathVariable("id") Integer id) {
        BizTestEntity bizTest = bizTestService.getById(id);

        return RestResponse.success().put("test", bizTest);
    }

    /**
     * 新增
     *
     * @param bizTest bizTest
     * @return RestResponse
     */
    @SysLog("新增")
    @RequestMapping("/save")
    @RequiresPermissions("biz:test:save")
    public RestResponse save(@RequestBody BizTestEntity bizTest) {
        bizTest.setId((int)(Math.random()*1000));
        bizTestService.add(bizTest);

        return RestResponse.success();
    }

    /**
     * 修改
     *
     * @param bizTest bizTest
     * @return RestResponse
     */
    @SysLog("修改")
    @RequestMapping("/update")
    @RequiresPermissions("biz:test:update")
    public RestResponse update(@RequestBody BizTestEntity bizTest) {

        bizTestService.update(bizTest);

        return RestResponse.success();
    }

    /**
     * 根据主键删除
     *
     * @param ids ids
     * @return RestResponse
     */
    @SysLog("删除")
    @RequestMapping("/delete")
    @RequiresPermissions("biz:test:delete")
    public RestResponse delete(@RequestBody Integer[] ids) {
        bizTestService.deleteBatch(ids);

        return RestResponse.success();
    }
}
