/*
 * 项目名称:mzzy-plus
 * 类名称:BizTestDao.java
 * 包名称:com.hackbuteer.mzzy.modules.biz.dao
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2021-06-05 16:00:57        高建文     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.hackbuteer.mzzy.modules.biz.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hackbuteer.mzzy.modules.biz.entity.BizTestEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Dao
 *
 * @author 高建文
 * @date 2021-06-05 16:00:57
 */
@Mapper
public interface BizTestDao extends BaseMapper<BizTestEntity> {

    /**
     * 查询所有列表
     *
     * @param params 查询参数
     * @return List
     */
    List<BizTestEntity> queryAll(@Param("params") Map<String, Object> params);

    /**
     * 自定义分页查询
     *
     * @param page   分页参数
     * @param params 查询参数
     * @return List
     */
    List<BizTestEntity> selectBizTestPage(IPage page, @Param("params") Map<String, Object> params);
}
