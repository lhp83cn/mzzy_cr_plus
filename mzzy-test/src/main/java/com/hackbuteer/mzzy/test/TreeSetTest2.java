package com.hackbuteer.mzzy.test;

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetTest2 {
    public static void main(String[] args) {
        TreeSet<Person> treeSet = new TreeSet<Person>(new MyComparator());

        Person person1 = new Person();
        person1.setId(1);
        person1.setName("zhangsan");
        person1.setScore(85);

        Person person2 = new Person();
        person2.setId(2);
        person2.setName("lisi");
        person2.setScore(89);

        treeSet.add(person1);
        treeSet.add(person2);

        System.out.println(treeSet);
    }
}

class Person {

    private int id;
    private String name;
    private int score;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}

class MyComparator implements Comparator<Person> {

    public int compare(Person o1, Person o2) {
        return o1.getId() - o1.getId();
    }
}
