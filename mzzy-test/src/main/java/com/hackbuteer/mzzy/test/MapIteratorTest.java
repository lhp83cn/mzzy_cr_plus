package com.hackbuteer.mzzy.test;

import java.util.*;

public class MapIteratorTest {

    public static void main(String[] args) {

        Map<String, String>  map = new HashMap<String, String>();
        map.put("1", "a");
        map.put("2", "b");
        map.put("3", "c");
        map.put("4", "d");
        map.put("5", "e");

        //遍历
        Set<Map.Entry<String, String>> sets = map.entrySet();

        for(Iterator<Map.Entry<String, String>> it = sets.iterator(); it.hasNext();) {
            Map.Entry<String, String> mapEntry = it.next();
            String key = mapEntry.getKey();
            String value = mapEntry.getValue();
            System.out.println("key:" + key + " value:" + value);
        }

        System.out.println("============================");

        //遍历2
        Collection<String> values = map.values();
        for(Iterator<String> valueTmp = values.iterator(); valueTmp.hasNext();) {
            String vTmp = valueTmp.next();
            System.out.println("value:" + vTmp);
        }

    }

}
