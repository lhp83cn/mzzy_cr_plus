package com.hackbuteer.mzzy.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class WordOfCountsTest {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<String, Integer>();

        for(int i = 0; i < args.length; i++) {

            String word = args[i];
            Integer wordCounts = map.get(word);

            if(null == wordCounts) {
                map.put(word, 1);
            } else {
                map.put(word, wordCounts+1);
            }

        }

        Set<Map.Entry<String, Integer>> sets = map.entrySet();
        for(Iterator<Map.Entry<String, Integer>> it = sets.iterator(); it.hasNext();) {
            Map.Entry<String, Integer> item = it.next();
            String key = item.getKey();
            Integer value = item.getValue();
            System.out.println("["+key+","+value+"]");
        }

    }

}
