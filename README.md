# mzzy_cr_plus

#### 介绍
化学试剂基础版本
SpringBoot2.1.2

#### 软件架构
1. 技术选型

后端
```
- Spring Boot 2.1.0.RELEASE
- Apache Shiro 1.4.0
- Spring MVC 5.1.2
- MyBatis 3.5.0、MyBatis-Plus 3.1.0
- weixin-java-mp 3.4.0
- weixin-java-miniapp 3.4.0
- weixin-java-pay 3.4.0
- weixin-java-open 3.4.0
- alipay-sdk 3.7.110.ALL
- Quartz 2.3.0
- Druid 1.1.10
- lombok 1.18.4
- swagger 2.9.2
- jwt 0.9.1
- easypoi 4.0.0
- Activiti6.0.0
```

前端
```
你需要在本地安装 nodejs。
- [nodejs]-------(http://nodejs.org/)
- [ES6]----------(http://es6.ruanyifeng.com/)
- [vue-cli]------(https://github.com/vuejs/vue-cli)
- [vue]----------(https://cn.vuejs.org/index.html 或 https://cn.vuejs.org/v2/api/)
- [vue-router]---(https://github.com/vuejs/vue-router)
- [vuex]---------(https://github.com/vuejs/vuex)
- [axios]--------(https://github.com/axios/axios)
- [vue-cookie]---(https://github.com/alfhen/vue-cookie)
- [element-ui]---(https://github.com/ElemeFE/element 或 http://element-cn.eleme.io/#/zh-CN/component/installation)
- [iconfont]-----(http://www.iconfont.cn/ 或 https://www.iconfont.cn/search/index)
- [echarts]]-----(https://www.echartsjs.com/api.html#echarts)
```

2.  项目结构

后端目录结构：
```
mzzy-plus
├─sql  项目SQL语句
│
├─mzzy-admin 管理后台(port:8888)
│ 
├─mzzy-api 接口服务(port:8889)
│ 
├─mzzy-biz 业务、数据处理
│ 
└─mzzy-common 公共类

```

前端目录结构：
```bash
├── build                      // 构建相关
├── config                     // 构建配置相关
├── dist                       // 构建打包生成部署文件
│   ├── 1902151513             // 静态资源（19年02月15日15时13分）
│   ├── config                 // 配置
│   ├── index.html             // index.html入口
├── src                        // 源代码
│   ├── assets                 // 静态资源
│   ├── components             // 全局公用组件
│   ├── element-ui             // element-ui组件配置
│   ├── element-ui-theme       // element-ui组件主题配置
│   ├── icons                  // 所有 svg icons
│   ├── router                 // 路由
│   ├── store                  // 全局 store管理
│   ├── utils                  // 全局公用方法
│   ├── views                  // view
│   ├── App.vue                // 入口组件
│   ├── main.js                // 入口
├── static                      // 第三方不打包资源
│   ├── config                 // 全局变量配置
│   ├── img                    // favicon图标
│   ├── plugins                // 插件
├── .babelrc                   // babel-loader 配置
├── eslintrc.js                // eslint 配置项
├── .gitignore                 // git 忽略项
├── index.html                 // html模板
└── package.json               // package.json
```


#### 安装教程
1.  后端部署
- 通过git下载源码
- 创建数据库plaftorm-plus（这里的数据库名称与配置文件匹配上就行，叫什么都无所谓）
- mysql执行sql/mysql.sql文件(oracle执行sql/oracle.sql)，初始化数据
- 修改admin、api模块下application-dev.yml，修改MySQL、Oracle驱动、账号和密码
- 运行PlatformAdminApplication.java启动后台管理接口服务
       
       -接口：http://localhost:8888/mzzy-admin
	   
- 运行PlatformApiApplication.java启动api接口服务

       -接口：http://localhost:8889/mzzy-api
        
- Swagger路径：

	   -接口：http://localhost:8889/mzzy-api/doc.html

2.  前端部署
```
npm install

npm run dev

```


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  常用API
- [Mybatis-Plus](https://baomidou.gitee.io/mybatis-plus-doc/#/quick-start)


2.  视频教程
- Vue学习视频：
	链接：https://pan.baidu.com/s/1PJL-UJQLPRgefbI8lPwD7A 提取码：lf8n 

